# docker-aienv

> This is personal testing repo. Not recommended to use.



## Usage

```sh
docker run --rm -d -p 8888:8888 --mount 'type=bind,src=/path/to/notebook,dst=/notebook' civalin/aienv
```

Then connect to <http://localhost:8888>.



## Usage (docker-compose)

Create `docker-compose.override.yaml`:

```yaml
version: "3.5"

services:
  aienv:
    # build: .
    image: civalin/aienv
    ports:
      - "8888:8888"
    volumes:
      - "/path/to/notebook/dir:/notebook"
```

Then run:

```sh
docker-compose up
```
