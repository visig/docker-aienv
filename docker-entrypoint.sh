#!/bin/bash

if [ -n "$LOCAL_USER_ID" ]; then
    GROUP_ID=${LOCAL_USER_ID}
    USER_ID=${LOCAL_USER_ID}
else
    GROUP_ID=$(stat -c %g `pwd`)
    USER_ID=$(stat -c %u `pwd`)
fi

groupadd -g $GROUP_ID -f user
useradd --shell /bin/bash -m -u $USER_ID -g $GROUP_ID -o -c "runner" user

mkdir -p /home/user/.jupyter
cp -rT /jupyter_config_template /home/user/.jupyter
chown -R user:user /home/user/.jupyter

echo "Starting with UID: $USER_ID"

exec /usr/local/bin/gosu user "$@"
